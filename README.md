# deadbeef-osx
Pre-built binary for deadbeef for MacOS X

Currently there are no pre-built MacOS X binaries for my favorite player deadbeef - because it is too limited in functionality. But I still like it and want to share with everyone who don't want to waste time downloading XCode and building.

Enjoy.

No warranties, but at least it works on my Mac.
